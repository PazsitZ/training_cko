<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <title>cko_training</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>

    <div class="title pull-right"><h2>Spring MySQL Hybernate and MongoDB</h2></div>
    <div class="jumbotron">
        <form:form method="post" action="formSubmit" commandName="entry">
            <div class="row">
                <div class="col-md-2"><form:label path="title">title:</form:label></div>
                <div class="col-md-2"><form:input path="title" /></div>
            </div>
            <div class="row">
                <div class="col-md-2"><label for="tagNames">tags:</label></div>
                <div class="col-md-2"><input name="tagNames" /></div>
            </div>
            <div class="row">
                <div class="col-md-2"><form:label path="content">content:</form:label></div>
                <div class="col-md-2"><form:textarea path="content" /></div>
            </div>
            <div class="row">
                <div class="col-md-1">
                    <input class="btn btn-default" type="submit" value="formSubmit"/>
                </div>
            </div>
        </form:form>
    </div>

    <div class="container">
        <h4>Entries from MySQL</h4>
        <c:if  test="${!empty entryList}">
            <c:forEach items="${entryList}" var="entry">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ${entry.title}
                        <span class="pull-right">
                        <c:forEach items="${entry.getTags()}" var="tag">
                            <button class="btn btn-default btn-xs" type="button"><span class="glyphicon glyphicon-tag"></span> ${tag.tagName}</button>
                        </c:forEach>
                        </span>
                    </div>
                    <div class="panel-body">${entry.content}</div>
                    <div class="panel-body"><a class="btn btn-default" href="delete/${entry.id}">delete</a></div>
                </div>
            </c:forEach>
        </c:if>

        <h4>Log Entries of Page Access from MongoDb</h4>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>IP Address</th>
                        <th>DateTime</th>
                        <th>Method</th>
                        <th>requestURI</th>
                        <th>UserAgent String</th>
                        <th>Delete Entry</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${logList}" var="log">
                    <tr>
                        <td>${log.ipAddress}</td>
                        <td>${log.datetime}</td>
                        <td>${log.request.method.toString()}</td>
                        <td>${log.request.requestUrl}</td>
                        <td>${log.userAgentString}</td>
                        <td><a class="btn btn-default" href="deleteLog/${log.id}">delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        <hr>

          <footer>
            <p>&copy; CKO training</p>
          </footer>
    </div>
</body>
</html>