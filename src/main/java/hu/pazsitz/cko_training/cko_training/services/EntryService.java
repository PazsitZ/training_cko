package hu.pazsitz.cko_training.cko_training.services;

import hu.pazsitz.cko_training.cko_training.daos.EntryDAO;
import hu.pazsitz.cko_training.cko_training.daos.TagDAO;
import hu.pazsitz.cko_training.cko_training.models.Entry;
import hu.pazsitz.cko_training.cko_training.models.Tag;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Service("EntryService")
@Transactional(readOnly = true)
public class EntryService {

    @Autowired
    private EntryDAO entryDao;
    @Autowired
    private TagDAO tagDao;

    public Entry findEntry(int id) {
        Entry entry = entryDao.findById(id);
        convertToMultiLine(entry);
        return entry;
    }

    public List<Entry> findAllEntry() {
        List<Entry> entries = entryDao.findAllEntry();
        for(Entry entry : entries) {
            convertToMultiLine(entry);
        }
        return entries;
    }

    @Transactional(readOnly = false)
    public void saveOrUpdateEntry(Entry entry) {
        entryDao.persist(entry);
    }

    @Transactional(readOnly = false)
    public void saveOrUpdateEntry(Entry entry, String tagNames) {
        buildTagsAndPutIntoEntry(entry, tagNames);
        saveOrUpdateEntry(entry);
    }

    @Transactional(readOnly = false)
    public void deleteEntry(int entryId) {
        entryDao.deleteEntry(entryId);
    }

    private void convertToMultiLine(Entry entry) {
        String lineSeparator = System.getProperty("line.separator");
        entry.setContent(
            entry.getContent().replace(lineSeparator, "<br />" + lineSeparator)
        );
    }

    private void buildTagsAndPutIntoEntry(Entry entry, String tagNames) {
        String[] tagArray = tagNames.split(",");
        for (String tagString : tagArray) {
            Tag tagEntity = tagDao.findByIdName(tagString.trim());
            if (tagEntity == null) {
                tagEntity = new Tag();
                tagEntity.setTagName(tagString.trim());
            }
            if (!StringUtils.isEmpty(tagEntity.getTagName())) {
                entry.getTags().add(tagEntity);
            }
        }
    }

}
