/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hu.pazsitz.cko_training.cko_training.services;

import hu.pazsitz.cko_training.cko_training.daos.LogDAO;
import hu.pazsitz.cko_training.cko_training.daos.RequestDAO;
import hu.pazsitz.cko_training.cko_training.models.AccessLog;
import hu.pazsitz.cko_training.cko_training.models.Request;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Service("AccessLogService")
public class AccessLogService {
    @Autowired
    private LogDAO logDao;
    @Autowired
    private RequestDAO requestDao;

    public ObjectId insert(AccessLog log) {
        ObjectId lOId = null;

        ObjectId rOId = requestDao.insert(log.getRequest());

        if (rOId != null) {
            lOId = logDao.insert(log);
        }

        return lOId;
    }

    public void delete(String id) {
        AccessLog al = logDao.selectByPk(id);
        if (al != null) {
            Request request = requestDao.selectByPk(al.getRequest().getId());
            if (request != null) {
                requestDao.delete(request.getId());
            }
            logDao.delete(al.getId());
        }
    }

    public List<AccessLog> getAllRecentLog() {
        return logDao.getAllRecentLog();
    }
}
