package hu.pazsitz.cko_training.cko_training.controllers;

import hu.pazsitz.cko_training.cko_training.models.Entry;
import hu.pazsitz.cko_training.cko_training.services.AccessLogService;
import hu.pazsitz.cko_training.cko_training.services.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Controller
@RequestMapping("/")
public class DefaultController {

    @Autowired
    private EntryService entryService;
    @Autowired
    private AccessLogService logService;

	@RequestMapping(value="/welcome/{name}", method = RequestMethod.GET)
	public String welcomeName(@PathVariable String name, ModelMap model) {

		model.addAttribute("message", "Spring MySQL Hybernate and MongoDB - " + name);
		return "index";

	}

    @RequestMapping(value = "/", method = RequestMethod.GET)
	public String form(ModelMap model) {
        model.addAttribute("entry", new Entry());
        model.addAttribute("entryList", entryService.findAllEntry());
        model.addAttribute("logList", logService.getAllRecentLog());

		return "form";

	}

    @RequestMapping(value = "/formSubmit", method = RequestMethod.POST)
	public String formSubmit(@ModelAttribute(value="entry") Entry entry, @RequestParam(value="tagNames", required = false) String tagNames, BindingResult result) {
        entryService.saveOrUpdateEntry(entry, tagNames);

		return "redirect:/";
	}

    @RequestMapping("/delete/{entryId}")
    public String deleteEntry(@PathVariable("entryId") int entryId)
    {
        entryService.deleteEntry(entryId);
        return "redirect:/";
    }

    @RequestMapping("/deleteLog/{entryId}")
    public String deleteLog(@PathVariable("entryId") String entryId)
    {
        logService.delete(entryId);
        return "redirect:/";
    }

}
