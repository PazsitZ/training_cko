package hu.pazsitz.cko_training.cko_training.models;

import java.io.Serializable;
import javax.persistence.Id;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Document(collection="AccessLog")
public class AccessLog implements Serializable {

    @Id
    private ObjectId id;

    private String ipAddress;
    private String datetime;
    @DBRef
    private Request request;
    private String userAgentString;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getUserAgentString() {
        return userAgentString;
    }

    public void setUserAgentString(String userAgentString) {
        this.userAgentString = userAgentString;
    }

    @Override
    public String toString() {
        return "AccessLog [id=" + id + ", logEntry=" + ipAddress + " " + datetime + " " + request.getMethod() + " " + request.getRequestUrl() + " " + userAgentString + "]";
    }

}
