package hu.pazsitz.cko_training.cko_training.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Entity
@Table(name = "entry")
public class Entry implements Serializable {

    @Id
    @GeneratedValue
	@Column(name = "id", nullable = false)
	private int id;

	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "content", nullable = false)
	private String content;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "entry_tag", joinColumns = {
        @JoinColumn(name = "entry_id", table = "entry", referencedColumnName = "id" ,nullable = false) }
        ,inverseJoinColumns = { @JoinColumn(name = "tag_id", table = "tag" ,referencedColumnName = "id", nullable = false) }
    )
    private Set<Tag> tags = new HashSet<>(0);

	public Entry() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


}
