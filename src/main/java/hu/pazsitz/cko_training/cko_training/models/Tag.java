package hu.pazsitz.cko_training.cko_training.models;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Entity
@Table(name = "tag")
public class Tag implements Serializable {
    @Id
    @GeneratedValue
	@Column(name = "id", nullable = false)
	private int id;

    @Column(unique = true)
    private String tagName;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags")
    private Set<Entry> entries;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Set<Entry> getEntries() {
        return entries;
    }

    public void setEntries(Set<Entry> entries) {
        this.entries = entries;
    }


}
