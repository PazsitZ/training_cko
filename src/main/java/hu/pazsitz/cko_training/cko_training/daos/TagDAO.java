/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hu.pazsitz.cko_training.cko_training.daos;

import hu.pazsitz.cko_training.cko_training.models.Entry;
import hu.pazsitz.cko_training.cko_training.models.Tag;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Repository("TagDAO")
public class TagDAO  extends AbstractDAO<Tag, Integer>{

    public TagDAO() {
        super(Tag.class);
    }

    public void persistEntry(Tag tag) {
        persist(tag);
	}

    public Tag findTagById(int id) {
        return findById(id);
	}

    public Tag findByIdName(String tagName) {
        List<Tag> tags = findByCriteria(Restrictions.like("tagName", tagName));
        return !tags.isEmpty() ? tags.get(0) : null;
    }
}
