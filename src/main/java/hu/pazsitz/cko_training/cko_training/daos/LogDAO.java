package hu.pazsitz.cko_training.cko_training.daos;

import hu.pazsitz.cko_training.cko_training.models.AccessLog;
import hu.pazsitz.cko_training.cko_training.models.Request;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Repository("LogDao")
public class LogDAO {

//    @Autowired
    protected MongoTemplate mongoTemplate;

    public void setMongoTemplate(MongoTemplate template) {
        this.mongoTemplate = template;
    }

    public ObjectId insert(AccessLog log) {
        mongoTemplate.insert(log);
        AccessLog al = (AccessLog)mongoTemplate.findById(log.getId(), AccessLog.class);

        return log.getId();
    }

    public boolean delete(ObjectId id) {
        return delete(id.toString());
    }

    public boolean delete(String id) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("id").is(id));
            mongoTemplate.remove(query, AccessLog.class);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<AccessLog> listLocalAccess() {
        Query query = new Query();
		query.addCriteria(Criteria.where("ipAddress").is("127.0.0.1"));
        return mongoTemplate.find(query, AccessLog.class);
    }

    public AccessLog selectByPk(String id) {
        return selectByPk(new ObjectId(id));
    }

    public AccessLog selectByPk(ObjectId id) {
        return (AccessLog)mongoTemplate.findById(id, AccessLog.class);
    }


    public List<AccessLog> getAllRecentLog() {
//        return (List<AccessLog>) mongoTemplate.findAll(AccessLog.class);

        Query query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "datetime"));
		query.limit(10);

        return (List<AccessLog>) mongoTemplate.find(query, AccessLog.class);
    }
}