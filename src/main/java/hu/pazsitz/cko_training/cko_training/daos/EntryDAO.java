package hu.pazsitz.cko_training.cko_training.daos;

import org.springframework.stereotype.Repository;
import hu.pazsitz.cko_training.cko_training.models.Entry;
import java.util.List;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Repository("EntryDAO")
public class EntryDAO extends AbstractDAO<Entry, Integer>{

    public EntryDAO() {
        super(Entry.class);
    }

    public void persistEntry(Entry entry) {
        persist(entry);
	}

    public Entry findEntryById(int id) {
        return findById(id);
	}

    public void updateEntry(Entry entry) {
        persist(entry);
	}

    public void deleteEntry(int entryId) {
        Entry entry = findById(entryId);
        if (null != entry) {
            delete(entry);
        }
	}

    public void deleteEntry(Entry entry) {
        delete(entry);
	}

    public List<Entry> findAllEntry() {
        List<Entry> entries = findByCriteria(Restrictions.gt("id", 0));
        return entries;
    }

}