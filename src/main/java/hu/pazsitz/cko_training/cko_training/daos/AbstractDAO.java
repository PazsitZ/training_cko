package hu.pazsitz.cko_training.cko_training.daos;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 * @param <E>
 * @param <I>
 */
public class AbstractDAO<E, I extends Serializable> {

    private final Class<E> entityClass;

    protected AbstractDAO(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    @Autowired
    private SessionFactory sessionFactory;

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public E findById(I id) {
        return (E) getCurrentSession().get(entityClass, id);
    }

    public void persist(E e) {
        getCurrentSession().saveOrUpdate(e);
    }

    public void delete(E e) {
        getCurrentSession().delete(e);
    }

    public List findByCriteria(Criterion criterion) {
        Criteria criteria = getCurrentSession().createCriteria(entityClass);
        criteria.add(criterion);
        return criteria.list();
    }
}
