/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hu.pazsitz.cko_training.cko_training.daos;

import hu.pazsitz.cko_training.cko_training.models.AccessLog;
import hu.pazsitz.cko_training.cko_training.models.Request;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Repository("RequestDao")
public class RequestDAO {

    protected MongoTemplate mongoTemplate;

    public void setMongoTemplate(MongoTemplate template) {
        this.mongoTemplate = template;
    }

    public Request selectByPk(String id) {
        return selectByPk(new ObjectId(id));
    }

    public Request selectByPk(ObjectId id) {
        return (Request)mongoTemplate.findById(id, Request.class);
    }

    public boolean delete(ObjectId id) {
        return delete(id.toString());
    }

    public boolean delete(String id) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("id").is(id));
            mongoTemplate.remove(query, Request.class);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ObjectId insert(Request request) {
        mongoTemplate.insert(request);
        request = (Request)mongoTemplate.findById(request.getId(), Request.class);

        return request.getId();
    }
}
