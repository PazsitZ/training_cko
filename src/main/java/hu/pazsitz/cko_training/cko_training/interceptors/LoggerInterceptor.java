package hu.pazsitz.cko_training.cko_training.interceptors;

import hu.pazsitz.cko_training.cko_training.models.AccessLog;
import hu.pazsitz.cko_training.cko_training.models.Request;
import hu.pazsitz.cko_training.cko_training.services.AccessLogService;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Zoltan Pazsit <pazsitz@pazsitz.hu>
 */
@Component
public class LoggerInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AccessLogService logService;

    public void setLogService(AccessLogService logService) {
        this.logService = logService;
    }

    @Override
    public boolean preHandle(HttpServletRequest httpRequest, HttpServletResponse response, Object handler) throws Exception  {
        AccessLog accessLog = new AccessLog();
        accessLog.setDatetime(getDateTime());
        accessLog.setIpAddress(getClientIpAddr(httpRequest));
        accessLog.setRequest(getRequest(httpRequest));
        accessLog.setUserAgentString(httpRequest.getHeader("user-agent"));

        logService.insert(accessLog);

        return super.preHandle(httpRequest, response, handler);
    }

    private String getDateTime() {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss Z");
        return dateFormat.format(now);
    }

    private Request getRequest(HttpServletRequest httpRequest) {
        Request request = new Request();
        request.setMethod(HttpMethod.valueOf(httpRequest.getMethod()));
        request.setRequestUrl(httpRequest.getRequestURI());
        return request;
    }

    private String getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        try {
            InetAddress.getByName(ip).getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(LoggerInterceptor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ip;
    }

}
